# APEX Y JET - GRÁFICOS JET PERSONALIZADOS

Exploraremos como aprovechar el potencial de Oracle JET para personalizar graficos en APEX usando la gran variedad de elementos presentes en el [JET Developer Cookbook](https://www.oracle.com/webfolder/technetwork/jet/jetCookbook.html).

Usaremos

- [Oracle APEX](https://apex.oracle.com/en/)
- [Oracle JET](https://www.oracle.com/webfolder/technetwork/jet/index.html)

Aqui puedes revisar la presentación.

[Presentación](https://slides.com/aflorestorres/oracle_apex_y_jet)

PD: Una vez importado la aplicación APEX, ir al menu admin y ejecutar el proceso de alli, esto permite cargar la data del webservice en las tablas de Oracle.
Pueden crear un JOB que se ejecute cada cierto tiempo y tengan la data actualizada.








 