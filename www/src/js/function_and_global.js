var world_data;
var samerica_data;
var africa;
var asia;
var australia;
var europe;
var nAmerica;

var newAmerica;
var covid_data;
 
function getJSON(url) {
      var resp ;
      var xmlHttp ;
      resp  = '' ;
      xmlHttp = new XMLHttpRequest(); 
      if(xmlHttp !== null)
      {
          xmlHttp.open( "GET", url, false );
          xmlHttp.send( null );
          resp = xmlHttp.responseText;
      }
      return resp ;
}


function formatNumber2(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')
}

function basic_pie_json(cases,tests,deaths,critical){
  return [
    {
        "id": 0,
        "series": "Casos",
        "group": "Group A",
        "value": cases
    },
    {
        "id": 1,
        "series": "Pruebas",
        "group": "Group A",
        "value": tests
    },
    {
        "id": 2,
        "series": "Muertos",
        "group": "Group A",
        "value": deaths
    },
    {
        "id": 3,
        "series": "Cuidados Intensivos",
        "group": "Group A",
        "value": critical	
    }
];
}


function init_pie_chart1(cases,tests,deaths,critical){ 
  
    require(['knockout', 'ojs/ojbootstrap', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojchart'
           ],
  function(ko, Bootstrap, ArrayDataProvider)
  {   

      function ChartModel() {
          this.threeDValue = ko.observable('off');
          this.formatValue = ko.observable('number'); 
          /* chart data */
         var pie_char_1 = ko.observableArray(basic_pie_json(cases,tests,deaths,critical)); 
          this.dataProvider = new ArrayDataProvider(pie_char_1, {keyAttributes: 'id'});
      }
      
      var chartModel = new ChartModel();
      
      Bootstrap.whenDocumentReady().then(
      function()
      {
          ko.applyBindings(chartModel, document.getElementById('chart-container1'));
      });
  });
  
}

function generar_tr(country,cases,tests,deaths,critical){
  var tr = "";
  tr+="<tr class='tr-covid' ><td class='td-covid' >" + country + "</td>";        
  tr+="<td class='td-covid' >" + formatNumber2(tests)  + "</td>";    
  tr+="<td class='td-covid' >" + formatNumber2(cases) + "</td>";    
  tr+="<td class='td-covid' >" + formatNumber2(deaths) + "</td>";    
  tr+="<td class='td-covid' >" + formatNumber2(critical) + "</td>";    
  tr+="</tr>"; 
  return tr;
}

function generar_tabla(){ 

var table_init = "<table id='data_covid' class='table-covid' > <thead> <tr class='tr-covid' >";
table_init+="<th class='th-covid' >Pais</th>";
table_init+="<th class='th-covid' >Pruebas</th>";
table_init+="<th class='th-covid' >Casos</th>";
table_init+="<th class='th-covid' >Muertos</th>";
table_init+="<th class='th-covid' >Crítico</th>";
table_init+="</tr> </thead><tbody id='data_covid_body'> </tbody> </table> ";
 return table_init; 
}
 


function init_pie_chart2(cases,tests,deaths,critical){ 
  
    require(['knockout', 'ojs/ojbootstrap', 'ojs/ojarraydataprovider', 'ojs/ojknockout', 'ojs/ojbutton', 'ojs/ojchart'
           ],
  function(ko, Bootstrap, ArrayDataProvider)
  {    
      function ChartModel() {
          this.threeDValue = ko.observable('off');
          this.formatValue = ko.observable('number'); 
          /* chart data */
         var pie_char_2 = ko.observableArray(basic_pie_json(cases,tests,deaths,critical)); 
          this.dataProvider = new ArrayDataProvider(pie_char_2, {keyAttributes: 'id'});
      }
      
      var chartModel = new ChartModel();
      
      Bootstrap.whenDocumentReady().then(
      function()
      {
          ko.applyBindings(chartModel, document.getElementById('chart-container2'));
      });
  });
  
}


function init_map_jet(){
   require(['knockout', 'ojs/ojbootstrap', 'ojs/ojattributegrouphandler', 'ojs/ojarraydataprovider', 
    'ojs/ojknockout', 'ojs/ojthematicmap', 'ojs/ojselectcombobox', 'ojs/ojlabel','ojs/ojmenu', 'ojs/ojlegend'], 
    function(ko, Bootstrap, attributeGroupHandler, ArrayDataProvider) {
        
      function DemoModel() {
        this.map = ko.observable('world');
        var areaData = ko.observableArray();
        this.dataProvider = new ArrayDataProvider(areaData, {keyAttributes: '@index'});
        this.mapProvider = ko.observable();
        var handler;
  
        this.getColor = function(id) {
          return handler.getValue(id);
        }; 

        var geo;
        var updateMap = function (map) {
          
          switch(map) {
            case 'africa':
              geo = set_infected(africa);
              break;
            case 'asia':
              geo = set_infected(asia);
              break;
            case 'australia':
              geo = set_infected(australia);
              break;
            case 'europe':
              geo = set_infected(europe);
              break;
            case 'northAmerica':
              geo = set_infected(nAmerica);
              break;
            case 'southAmerica':
              geo = set_infected(samerica_data);
              break;
            case 'world':
              geo =  set_infected(world_data);
              break;
            default: 
              geo =  set_infected(world_data);// JSON.parse(world_data);
          }
          this.mapProvider({
            geo: geo,
            propertiesKeys: {
              id: 'iso_a3',
              shortLabel: 'iso_a3',
              longLabel: 'name_long'
            }
          });
  
          areaData(geo["features"]);
          // For demo purposes, use the color attribute group handler to give
          // areas different colors based on a non important data dimension.
          // In a real application, the color attribute group handler should be
          // passed a meaningful data dimension.
          handler = new attributeGroupHandler.ColorAttributeGroupHandler();
        }.bind(this);
        
        var country_name;
        var country_code;
        var selected_cases;
        var selected_test;
        var selected_deaths;
        var selected_critical;

         this.beforeOpenFunction = function (event) {
            var target = event.detail.originalEvent.target;
            var thematicMap = document.getElementById("map1");
            var context = thematicMap.getContextByNode(target);
            country_name = null;
            if (context != null) {
              if (context.subId == "oj-thematicmap-area")                
                country_name = geo["features"][context["index"]].properties.name_long;
                country_code = geo["features"][context["index"]].properties.iso_a3;
                selected_cases = geo["features"][context["index"]].properties.population_infected;
                selected_test = geo["features"][context["index"]].properties.tests;
                selected_deaths = geo["features"][context["index"]].properties.deaths;
                selected_critical = geo["features"][context["index"]].properties.critical;
            }
          };
          

          var range_infections1 = 1000;
          var range_infections2 = 10000;
          var range_infections3 = 100000;
          var range_infections4 = 1000000; 

          this.getRainfallColor = function(total_cases) {
            // Bucket rainfall data into categories to color
            let l_cases = parseInt(total_cases, 10)
            if (l_cases <= range_infections1)
              return '#faa307';
            else if (l_cases <= range_infections2)
              return '#f48c06';
            else if (l_cases <= range_infections3)
              return '#e85d04';
            else if (l_cases <= range_infections4)
              return '#dc2f02';
            else if (l_cases > range_infections4)
              return '#d00000';
            else 
              return '#edf6f9';
          };
  
         var legendSections = [{items: [
           {text: "0-"+range_infections1, color: '#faa307'},
           {text: range_infections1+"-"+range_infections2, color: '#f48c06'},
           {text: range_infections2+"-"+range_infections3, color: '#e85d04'},
           {text: range_infections3+"-"+range_infections4, color: '#dc2f02'},
           {text: range_infections4+"+", color: '#d00000'},
         ]}];
         this.legendDataProvider = new ArrayDataProvider(legendSections, {keyAttributes: 'text'});
        
        
         this.menuItemAction = function (event) {
            var text = event.target.textContent;
            var value = event.target.value;
            console.log('event.target',event.target)
            if (country_name && value == 'action1') {
              //this.selectedMenuItem(text + " from " + state.State);
              console.log(text + '  ' + country_code);
              if (!apex.item("P100_COUNTRY_1").getValue()){
                apex.item("P100_COUNTRY_1").setValue(country_name);  
                $("#idInfecionesCovid").show();
                $("#sr_covid_infections").parent().removeClass('col-12');
                $("#sr_covid_infections").parent().addClass('col-7'); 
                init_pie_chart1(selected_cases,selected_test,selected_deaths,selected_critical);

                let tb = generar_tabla();
                $('#distribucion_covid').append(tb);
                let tr = generar_tr(country_name,selected_cases,selected_test,selected_deaths,selected_critical);
                $('#data_covid_body').append(tr);

              }else{ if (!apex.item("P100_COUNTRY_2").getValue()){
                apex.item("P100_COUNTRY_2").setValue(country_name);  
                $("#pie_region_2").show();  
                init_pie_chart2(selected_cases,selected_test,selected_deaths,selected_critical);
                
                let tr = generar_tr(country_name,selected_cases,selected_test,selected_deaths,selected_critical);
                $('#data_covid_body').append(tr);

                }else{
                  let tr = generar_tr(country_name,selected_cases,selected_test,selected_deaths,selected_critical);
                  $('#data_covid_body').append(tr);  
                }
               
              }
              
            } else {
              //this.selectedMenuItem(text + " from Thematic Map background");
              console.log(text + '2');
              
            }
          }.bind(this);
         
        updateMap(this.map());
  
        this.mapListener = function (event) {
          updateMap(event.detail.value);
        };
      }
  
      Bootstrap.whenDocumentReady().then(
      function() {
          ko.applyBindings(new DemoModel(), document.getElementById('mapdemo'));
        }
      );
  
    });
}

function set_infected(map_scr){

    var map_json = JSON.parse(map_scr);
    var i= 0;
    var random = "0";
    let infections;
    let test;
    let deaths;
    let critical; 
    let country_iso ;
    for(i=0; i<=map_json["features"].length-1; i++){ 
    country_iso = map_json["features"][i].properties["iso_a3"]; 
    
    try{
    infections = covid_data.find(x => x.iso3 == country_iso).cases; 
    test = covid_data.find(x => x.iso3 == country_iso).tests; 
    deaths = covid_data.find(x => x.iso3 == country_iso).deaths; 
    critical = covid_data.find(x => x.iso3 == country_iso).critical; 
    }
    catch(err) {
        infections = 0;
    }
    
    map_json["features"][i].properties["population_infected"] = infections; 
    map_json["features"][i].properties["tests"] = test; 
    map_json["features"][i].properties["deaths"] = deaths; 
    map_json["features"][i].properties["critical"] = critical; 

    }
    //map_scr = JSON.stringify(map_json);
    return map_json;
}

function init_world_map_data(){
  
  world_data = getJSON('#APP_IMAGES#world_countries.json') ;
  africa = getJSON('#APP_IMAGES#africa_countries.json');
  asia = getJSON('	#APP_IMAGES#asia_countries.json');
  australia = getJSON('#APP_IMAGES#australia_coutries.json');
  europe = getJSON('#APP_IMAGES#europe_countries.json');
  nAmerica = getJSON('#APP_IMAGES#north_america_countries.json');
  samerica_data = getJSON('#APP_IMAGES#south_america_countries.json'); 
  
}

function get_covid_data(){
  
   init_require();
   init_world_map_data();

  apex.server.process("GET_COVID_DATA",        
  {  
  }, 
    { success: function(pData){  
        console.log(pData);
        covid_data = pData.covid_infections;
        init_map_jet();
      }, 
      error: function(e) {   
           console.log(e);   
      } 
    } 
  //, {dataType: "text"}
  );
};