var world_data;
var samerica_data;
var africa;
var asia;
var australia;
var europe;
var nAmerica;

var newAmerica;
var covid_data;

function getJSON(url) {
      var resp ;
      var xmlHttp ;
      resp  = '' ;
      xmlHttp = new XMLHttpRequest(); 
      if(xmlHttp !== null)
      {
          xmlHttp.open( "GET", url, false );
          xmlHttp.send( null );
          resp = xmlHttp.responseText;
      }
      return resp ;
  }


function init_map(){
   require(['knockout', 'ojs/ojbootstrap', 'ojs/ojattributegrouphandler', 'ojs/ojarraydataprovider', 
    'ojs/ojknockout', 'ojs/ojthematicmap', 'ojs/ojselectcombobox', 'ojs/ojlabel','ojs/ojmenu'], 
    function(ko, Bootstrap, attributeGroupHandler, ArrayDataProvider) {
       
   
    //console.log('sAmerica',sAmerica);
    //console.log('world',world);
   
      function DemoModel() {
        this.map = ko.observable('world');
        var areaData = ko.observableArray();
        this.dataProvider = new ArrayDataProvider(areaData, {keyAttributes: '@index'});
        this.mapProvider = ko.observable();
        var handler;
  
        this.getColor = function(id) {
          return handler.getValue(id);
        };

      this.getRainfallColor = function(rainfall) {
          // Bucket rainfall data into categories to color
          if (rainfall <= 1000)
            return '#ffcccc';
          else if (rainfall <= 10000)
            return '#ff8383';
          else if (rainfall <= 30000)
            return '#ff4545';
          else if (rainfall <= 64000)
            return '#ff0000';
          else
            return '#0D4F8B';
        };

        var geo;
        var updateMap = function (map) {
          
          switch(map) {
            case 'africa':
              geo = JSON.parse(africa);
              break;
            case 'asia':
              geo = JSON.parse(asia);
              break;
            case 'australia':
              geo = JSON.parse(australia);
              break;
            case 'europe':
              geo = JSON.parse(europe);
              break;
            case 'northAmerica':
              geo = JSON.parse(nAmerica);
              break;
            case 'southAmerica':
              geo = JSON.parse(samerica_data);
              break;
            case 'world':
              geo = JSON.parse(world_data);
              break;
            default:
              geo = JSON.parse(world_data);
          }
          this.mapProvider({
            geo: geo,
            propertiesKeys: {
              id: 'iso_a3',
              shortLabel: 'iso_a3',
              longLabel: 'name_long'
            }
          });
  
          areaData(geo["features"]);
          // For demo purposes, use the color attribute group handler to give
          // areas different colors based on a non important data dimension.
          // In a real application, the color attribute group handler should be
          // passed a meaningful data dimension.
          handler = new attributeGroupHandler.ColorAttributeGroupHandler();
        }.bind(this);
        
        var state;
         this.beforeOpenFunction = function (event) {
            var target = event.detail.originalEvent.target;
            var thematicMap = document.getElementById("map1");
            var context = thematicMap.getContextByNode(target);
            state = null;
            if (context != null) {
              if (context.subId == "oj-thematicmap-area")
                console.log(context["index"]);
                state = geo["features"][context["index"]].properties.name_long;
                console.log(state);
            }
          };
        
         this.menuItemAction = function (event) {
            var text = event.target.textContent;
            if (state) {
              //this.selectedMenuItem(text + " from " + state.State);
              console.log(text + '1');
            } else {
              //this.selectedMenuItem(text + " from Thematic Map background");
              console.log(text + '2');
              
            }
          }.bind(this);
        
        
        updateMap(this.map());
  
        this.mapListener = function (event) {
          updateMap(event.detail.value);
        };
      }
  
      Bootstrap.whenDocumentReady().then(
      function() {
          ko.applyBindings(new DemoModel(), document.getElementById('mapdemo'));
        }
      );
  
    });
}


function init_world_map(){
  
world_data = getJSON('#APP_IMAGES#world_countries.json') ;
africa = getJSON('#APP_IMAGES#africa_countries.json');
asia = getJSON('	#APP_IMAGES#asia_countries.json');
australia = getJSON('#APP_IMAGES#australia_coutries.json');
europe = getJSON('#APP_IMAGES#europe_countries.json');
nAmerica = getJSON('#APP_IMAGES#north_america_countries.json');
samerica_data = getJSON('#APP_IMAGES#south_america_countries.json');


newAmerica = JSON.parse(world_data);
var i= 0;
var random = "0";
let infections;
let country_iso ;
for(i=0; i<=newAmerica["features"].length-1; i++){
//for(i=0; i<= 2 ; i++){
  
  //random = Math.floor((Math.random() * 64000) + 1);
  country_iso = newAmerica["features"][i].properties["iso_a3"];
 // console.log('country_iso',country_iso);
  
  try{
  infections = covid_data.find(x => x.iso3 == country_iso).cases;
  //  console.log('infections',infections);
  }
  catch(err) {
    infections = 0;
  }
  
  newAmerica["features"][i].properties["population_infected"] = infections;

}
world_data = JSON.stringify(newAmerica);


init_map();
  
}

function get_covid_data(){
  
   init_require();
  
  apex.server.process("GET_COVID_DATA",        
  {  
  }, 
    { success: function(pData){  
        console.log(pData);
        covid_data = pData.covid_infections;
        init_world_map();
      }, 
      error: function(e) {   
           console.log(e);   
      } 
    }

  //, {dataType: "text"}
  );
};