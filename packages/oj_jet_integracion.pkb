create or replace package body oj_jet_integracion is

procedure covid_infections(in_data_date  in date)
as
  l_cursor           sys_refcursor;
begin
    open l_cursor for  select cases  "cases"
                            , tests  "tests"
                            , active  "active"
                            , deaths  "deaths"
                            , country  "country"
                            , updated_data  "updated_data"
                            , critical  "critical"
                            , continent  "continent"
                            , recovered  "recovered"
                            , todaycases  "todaycases"
                            , country_id  "country_id"
                            , latitud  "latitud"
                            , flag  "flag"
                            , iso2  "iso2"
                            , iso3  "iso3"
                            , longitud  "longitud"
                            , todaydeaths  "todaydeaths"
                            , casesperonemillion  "casesperonemillion"
                            , testsperonemillion  "testsperonemillion"
                            , deathsperonemillion  "deathsperonemillion"
									       from oj_novelcovid
										  	where trunc(data_date) = trunc(in_data_date) ; 
    
  apex_json.open_object;
  apex_json. write('covid_infections', l_cursor);
  apex_json.close_object;

end covid_infections;


procedure covid_infections_top(in_data_date  in date
                              ,in_count      in number
                              ,millones_yn   in  varchar2 default 'N')
as
  l_cursor           sys_refcursor;
begin
    open l_cursor for  select s1.cases/case when millones_yn = 'Y' then 1000000 else 1 end  "cases"
                            , s1.tests/case when millones_yn = 'Y' then 1000000 else 1 end  "tests"
                            , s1.active  "active"
                            , s1.deaths  "deaths"
                            , s1.country  "country"
                            , s1.updated_data  "updated_data"
                            , s1.critical  "critical"
                            , s1.continent  "continent"
                            , s1.recovered/case when millones_yn = 'Y' then 1000000 else 1 end  "recovered"
                            , s1.todaycases  "todaycases"
                            , s1.country_id  "country_id"
                            , s1.latitud  "latitud"
                            , s1.flag  "flag"
                            , s1.iso2  "iso2"
                            , s1.iso3  "iso3"
                            , s1.longitud  "longitud"
                            , s1.todaydeaths  "todaydeaths"
                            , s1.casesperonemillion  "casesperonemillion"
                            , s1.testsperonemillion  "testsperonemillion"
                            , s1.deathsperonemillion  "deathsperonemillion"
									       from (
                            select nc.*
                              from oj_novelcovid nc
                             where trunc(data_date) = trunc(in_data_date) 
                             order by nc.tests desc 
                         ) s1 
                         where rownum <= in_count; 
    
  apex_json.open_object;
  apex_json. write('covid_infections', l_cursor);
  apex_json.close_object;

end covid_infections_top;




procedure populate_novel_covid(in_data_date   in   date
                              ,out_rows_loaded out varchar2)
as
  l_response clob; 
  l_json apex_json.t_values;
  l_paths apex_t_varchar2;
  
  l_count   number; 
 
  l_cases                   oj_novelcovid.cases%type;
  l_tests                   oj_novelcovid.tests%type;
  l_active                  oj_novelcovid.active%type;
  l_deaths                  oj_novelcovid.deaths%type;
  l_country                 oj_novelcovid.country%type;
  l_updated                 varchar2(500); 
  l_critical                oj_novelcovid.critical%type;
  l_continent               oj_novelcovid.continent%type;
  l_recovered               oj_novelcovid.recovered%type;
  l_todaycases              oj_novelcovid.todaycases%type;
  l_country_id              oj_novelcovid.country_id%type;
  l_latitud                 oj_novelcovid.latitud%type;
  l_flag                    oj_novelcovid.flag%type;
  l_iso2                    oj_novelcovid.iso2%type;
  l_iso3                    oj_novelcovid.iso3%type;
  l_longitud                oj_novelcovid.longitud%type;
  l_todaydeaths             oj_novelcovid.todaydeaths%type;
  l_casesperonemillion      oj_novelcovid.casesperonemillion%type;
  l_testsperonemillion      oj_novelcovid.testsperonemillion%type;
  l_deathsperonemillion     oj_novelcovid.deathsperonemillion%type;
  l_data_date               oj_novelcovid.data_date%type;

begin

  delete from oj_novelcovid where trunc(data_date) = trunc(in_data_date); 

  l_response := apex_web_service.make_rest_request (
        p_url => 'https://corona.lmao.ninja/v2/countries'
      , p_http_method => 'GET' 
  );
  
  apex_json.parse(l_json, l_response); 
  l_count := apex_json.get_count(p_path=>'.',p_values=>l_json);

  --DBMS_OUTPUT.put_line('asf:'|| l_count);
  for i in 1..l_count loop  
  
  l_cases := apex_json.get_varchar2(p_path=>'[%d].cases',p_values=>l_json,p0 => i); 
  l_tests := apex_json.get_varchar2(p_path=>'[%d].tests',p_values=>l_json,p0 => i); 
  l_active := apex_json.get_varchar2(p_path=>'[%d].active',p_values=>l_json,p0 => i); 
  l_deaths := apex_json.get_varchar2(p_path=>'[%d].deaths',p_values=>l_json,p0 => i); 
  l_country := apex_json.get_varchar2(p_path=>'[%d].country',p_values=>l_json,p0 => i); 
  l_updated := apex_json.get_varchar2(p_path=>'[%d].updated',p_values=>l_json,p0 => i); 
 -- l_updated_date := apex_json.get_date(p_path=>'[%d].updated',p_values=>l_json,p0 => i); 
  l_critical := apex_json.get_varchar2(p_path=>'[%d].critical',p_values=>l_json,p0 => i); 
  l_continent := apex_json.get_varchar2(p_path=>'[%d].continent',p_values=>l_json,p0 => i); 
  l_recovered := apex_json.get_varchar2(p_path=>'[%d].recovered',p_values=>l_json,p0 => i); 
  l_todaycases := apex_json.get_varchar2(p_path=>'[%d].todayCases',p_values=>l_json,p0 => i); 
  l_country_id := apex_json.get_varchar2(p_path=>'[%d].countryInfo._id',p_values=>l_json,p0 => i); 
  l_latitud := apex_json.get_varchar2(p_path=>'[%d].countryInfo.lat',p_values=>l_json,p0 => i); 
  l_longitud := apex_json.get_varchar2(p_path=>'[%d].countryInfo.long',p_values=>l_json,p0 => i); 
  l_flag := apex_json.get_varchar2(p_path=>'[%d].countryInfo.flag',p_values=>l_json,p0 => i); 
  l_iso2 := apex_json.get_varchar2(p_path=>'[%d].countryInfo.iso2',p_values=>l_json,p0 => i); 
  l_iso3 := apex_json.get_varchar2(p_path=>'[%d].countryInfo.iso3',p_values=>l_json,p0 => i); 
  l_todaydeaths := apex_json.get_varchar2(p_path=>'[%d].todayDeaths',p_values=>l_json,p0 => i); 
  l_casesperonemillion := apex_json.get_varchar2(p_path=>'[%d].casesPerOneMillion',p_values=>l_json,p0 => i); 
  l_testsperonemillion := apex_json.get_varchar2(p_path=>'[%d].testsPerOneMillion',p_values=>l_json,p0 => i); 
  l_deathsperonemillion := apex_json.get_varchar2(p_path=>'[%d].deathsPerOneMillion',p_values=>l_json,p0 => i);   
-- DBMS_OUTPUT.put_line(l_updated);
 -- DBMS_OUTPUT.put_line(l_updated_date);
 
insert
  into oj_novelcovid ( 
       cases
     , tests
     , active
     , deaths
     , country
     , updated_data
     , critical
     , continent
     , recovered
     , todaycases
     , country_id
     , latitud
     , flag
     , iso2
     , iso3
     , longitud
     , todaydeaths
     , casesperonemillion
     , testsperonemillion
     , deathsperonemillion
     --, data_date
     ) values (
       l_cases
     , l_tests
     , l_active
     , l_deaths
     , l_country
     , l_updated
     , l_critical
     , l_continent
     , l_recovered
     , l_todaycases
     , l_country_id
     , l_latitud
     , l_flag
     , l_iso2
     , l_iso3
     , l_longitud
     , l_todaydeaths
     , l_casesperonemillion
     , l_testsperonemillion
     , l_deathsperonemillion
     --, l_data_date
      ); 

  end loop; 
   
  out_rows_loaded := l_count;
  
  
end populate_novel_covid;

end oj_jet_integracion;
