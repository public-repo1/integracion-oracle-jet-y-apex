create or replace package oj_jet_integracion as

procedure covid_infections(in_data_date  in date);

procedure covid_infections_top(in_data_date  in date
                              ,in_count      in number
                              ,millones_yn   in  varchar2 default 'N');


procedure populate_novel_covid(in_data_date   in   date
                              ,out_rows_loaded out varchar2);



end oj_jet_integracion;
