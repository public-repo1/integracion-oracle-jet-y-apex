# Pasos Integración Jet

1.  Importar en static files top_de_pruebas.json y libraries_jet_8.3.js 

    - #APP_IMAGES#libraries_jet_8.3.js
    - #APP_IMAGES#top_de_pruebas.json

1. Crear página 201

1. Crear proceso con:

    APEX_JAVASCRIPT.add_requirejs();

1. Librerias JS

    #APP_IMAGES#libraries_jet_8.3.js

1. Librerias CSS

    #JET_CSS_DIRECTORY#/alta/oj-alta-notag-min.css

1. CookBook

    [PictoChart](https://www.oracle.com/webfolder/technetwork/jet/jetCookbook.html?component=pictoChart&demo=fractions)


1. Crear nueva region y copiar div

     Top Pruebas realizadas Covid-19

```html
<div id="sampleDemo" style="" class="demo-padding demo-container">
        <div id="componentDemoContent" style="width: 1px; min-width: 100%;">
                  
          <div id='chart-container' style="width:auto;">
            <h1>Top Pruebas realizadas Covid-19</h1>
            <div >
              <oj-table id="table" aria-label="Top Pruebas realizadas Covid-19"
                                data='[[dataProvider]]'
                                columns-default='{"sortable": "enabled"}' 
                                display='grid'
                                columns='{{columns}}'
                                style='width: 100%;'>
                <template slot="pictoChartCellTemplate" data-oj-as="cell">
                  <oj-picto-chart id="pictochart1" 
                                data="[[getChartDataProvider(cell)]]"
                                row-count="1" 
                                tooltip.renderer="[[tooltipFunction]]">
                    <template slot="itemTemplate" data-oj-as="item">
                      <oj-picto-chart-item name="[[item.data.name]]" color="[[item.data.color]]" count="[[item.data.count]]"></oj-picto-chart-item>
                    </template>
                  </oj-picto-chart>
                </template>
              </oj-table>
            </div>
            <div style="font-weight:bold;font-size:15px;margin-top:20px;">Legend</div>
            <oj-legend id="legend1"
              orientation="horizontal"
              symbol-width="20" symbol-height="20"
              data="[[legendDataProvider]]"
              style="height:70px;">
              <template slot="itemTemplate" data-oj-as="item">
                <oj-legend-item text="[[item.data.text]]"></oj-legend-item>
              </template>
            </oj-legend>
          </div> 
          
        </div>
      </div>
```

9. Agregar JavaScript

```JS
require(['knockout', 'ojs/ojbootstrap', 'ojs/ojarraydataprovider', 'ojs/ojarraytreedataprovider', 'ojs/ojattributegrouphandler',
    'ojs/ojknockout', 'ojs/ojpictochart', 'ojs/ojbutton', 'ojs/ojtable', 'ojs/ojlegend'],
    function (ko, Bootstrap, ArrayDataProvider, ArrayTreeDataProvider, attributeGroupHandler) {
      function PictoChartModel() {
        var colorHandler = new attributeGroupHandler.ColorAttributeGroupHandler();
        var deptArray = JSON.parse(tableData);
        this.dataProvider = new ArrayDataProvider(deptArray, {keyAttributes: 'country'});
        this.columns = [{"headerText": "PaPaísis","field": "country"},
                        {"headerText": "Pruebas","field": "tests", "template": "pictoChartCellTemplate"},
                        {"headerText": "Casos","field": "cases", "template": "pictoChartCellTemplate"},
                        {"headerText": "Recuperados","field": "recovered", "template": "pictoChartCellTemplate"},
                        {"headerText": "Crítico","field": "critical"}];
        this.tooltipFunction = function (dataContext) {
          var amount = dataContext.count;
          var tooltipElem = dataContext.id +" : "+amount+" millones"; //("+dataContext.name +")
          return {'insert':tooltipElem};
        }
  
        this.getChartDataProvider = function(cell){
          //console.log(cell);
          var item = [{id: cell.row.country, name: Object.keys(cell.row)[cell.columnIndex], count: cell.data, color: colorHandler.getValue(cell.row.country)}];
          return new ArrayDataProvider(item, {keyAttributes: "id"});
        }
  
        var legendItems = [{items: [{"text": "1 Millon"}]}];
        this.legendDataProvider = new ArrayTreeDataProvider(legendItems, {keyAttributes: "text", childrenAttribute: "items"});
      }
  
      var pictoChartModel = new PictoChartModel();
  
      Bootstrap.whenDocumentReady().then(
      function() {
          ko.applyBindings(pictoChartModel, document.getElementById('chart-container'));
        }
      );
    });
```

10. Declarar variable y agregar función para obtener el JSON
    Remover tableData

```js
var tableData;

function getJSON(url) {
      var resp ;
      var xmlHttp ;
      resp  = '' ;
      xmlHttp = new XMLHttpRequest(); 
      if(xmlHttp !== null)
      {
          xmlHttp.open( "GET", url, false );
          xmlHttp.send( null );
          resp = xmlHttp.responseText;
      }
      return resp ;
}
 
```

11. Inicializar chart.

```js
  tableData = getJSON('#APP_IMAGES#top_de_pruebas.json') ; 
  
  // librerias JET
  init_require();
  
  init_chart();
```

