 function _getCDNPath(paths) {
    var cdnPath = 'https://static.oracle.com/cdn/jet/';
    var ojPath = 'v8.3.0/default/js/';
    var thirdpartyPath = 'v8.3.0/3rdparty/';
    var keys = Object.keys(paths);
    var newPaths = {};
    function _isoj(key) {
      return (key.indexOf('oj') === 0 && key !== 'ojdnd');
    }
    keys.forEach(function (key) {
      newPaths[key] = cdnPath + (_isoj(key) ? ojPath : thirdpartyPath) + paths[key];
    });
    return newPaths;
  }

function init_require(){
  
  require.config({
    paths: _getCDNPath({
      knockout: 'knockout/knockout-3.5.0',
      jquery: 'jquery/jquery-3.5.1.min',
      'jqueryui-amd': 'jquery/jqueryui-amd-1.12.1.min',
      promise: 'es6-promise/es6-promise.min',
      ojs: 'debug',
      ojL10n: 'ojL10n',
      ojtranslations: 'resources',
      signals: 'js-signals/signals.min',
      text: 'require/text',
      hammerjs: 'hammer/hammer-2.0.8.min',
      ojdnd: 'dnd-polyfill/dnd-polyfill-1.0.1.min',
      customElements: 'webcomponents/custom-elements.min',
      touchr: 'touchr/touchr',
      rxjs: 'rxjs/rxjs.umd.min',
      css: 'require-css/css.min'
    })
  });
  
  // eslint-disable-next-line no-undef
  requirejs.config({
    baseUrl: '../js',
      // Path mappings for the logical module names
    paths: {
    },
      // Shim configurations for modules that do not expose AMD
    shim: {
      jquery: {
        exports: ['jQuery', '$']
      },
      maps: {
        deps: ['jquery', 'i18n'],
      }
    },
      // This section configures the i18n plugin. It is merging the Oracle JET built-in translation
      // resources with a custom translation file.
      // Any resource file added, must be placed under a directory named "nls". You can use a path mapping or you can define
      // a path that is relative to the location of this main.js file.
    config: { 
    }
  });
  
};